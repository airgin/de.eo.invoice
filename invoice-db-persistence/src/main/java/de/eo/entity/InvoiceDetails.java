package de.eo.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import lombok.Data;

@Entity
@Data
@Table(name = "INVOICE_DETAILS_T")
public class InvoiceDetails implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id;

	private LocalDate fromDate;

	private LocalDate toDate;

	private DoubleProperty amountPerUnit = new SimpleDoubleProperty();

	private StringProperty description = new SimpleStringProperty();

	private DoubleProperty unitPrice = new SimpleDoubleProperty();

	private DoubleProperty price = new SimpleDoubleProperty();

}
