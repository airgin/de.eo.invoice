package de.eo.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import lombok.Data;

@Entity
@Data
@Table(name = "INVOICE_T")
public class Invoice implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -8157681557288067324L;

	@Id
	@GeneratedValue
	private int id;

	@Column(nullable = false)
	private StringProperty number = new SimpleStringProperty();

	private StringProperty receiver = new SimpleStringProperty();;

	private ObjectProperty<LocalDate> date = new SimpleObjectProperty<LocalDate>();

	private DoubleProperty netInvoice = new SimpleDoubleProperty();

	private DoubleProperty vat = new SimpleDoubleProperty();

	private DoubleProperty grossInvoice = new SimpleDoubleProperty();

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<InvoiceDetails> invoiceDetailsList;

}
