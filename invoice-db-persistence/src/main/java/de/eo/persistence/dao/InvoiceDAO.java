package de.eo.persistence.dao;

import de.eo.entity.Invoice;

public class InvoiceDAO extends AbstractDAO<Invoice>{

	public InvoiceDAO() {
		super(Invoice.class);
	}

}
