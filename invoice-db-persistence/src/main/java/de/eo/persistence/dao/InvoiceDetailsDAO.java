package de.eo.persistence.dao;

import de.eo.entity.InvoiceDetails;

public class InvoiceDetailsDAO extends AbstractDAO<InvoiceDetails> {

	public InvoiceDetailsDAO() {
		super(InvoiceDetails.class);
	}

}
