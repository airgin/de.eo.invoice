package de.eo.persistence.dao;

import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

public abstract class AbstractDAO<T> {

	private final Class<T> entityType;

	private static EntityManagerFactory emFactory = Persistence.createEntityManagerFactory("invoice");

	public AbstractDAO(Class<T> type) {
		this.entityType = type;
	}

	protected EntityManager getEntityManager() {
		return emFactory.createEntityManager();
	}

	public List<T> selectAll() {
		EntityManager em = emFactory.createEntityManager();

		Query query = em.createQuery("SELECT t FROM " + entityType.getSimpleName() + " t");
		List<T> resultList = query.getResultList();
		em.close();
		return resultList;
	}

	public void persist(T t) {
		EntityManager em = emFactory.createEntityManager();
		EntityTransaction transaction = em.getTransaction();
		transaction.begin();
		em.persist(t);
		transaction.commit();
		em.close();
	}

	public void remove(T t) {
		EntityManager em = emFactory.createEntityManager();
		EntityTransaction transaction = em.getTransaction();
		transaction.begin();
		if (!em.contains(t)) {
			t = em.merge(t);
		}
		em.remove(t);
		transaction.commit();
		em.close();
	}

	public void remove(Collection<T> entitys) {
		EntityManager em = emFactory.createEntityManager();
		EntityTransaction transaction = em.getTransaction();
		transaction.begin();
		for (T entity : entitys) {
			if (!em.contains(entity)) {
				entity = em.merge(entity);
			}
			em.remove(entity);
		}
		transaction.commit();
		em.close();
	}
}
