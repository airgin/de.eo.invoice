package de.eo.persistence.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

@Converter(autoApply = true)
public class StringPropertyConverter implements AttributeConverter<StringProperty, String> {

	@Override
	public String convertToDatabaseColumn(StringProperty attribute) {
		// TODO Auto-generated method stub
		return attribute.get();
	}

	@Override
	public StringProperty convertToEntityAttribute(String dbData) {
		return new SimpleStringProperty(dbData);
	}

}
