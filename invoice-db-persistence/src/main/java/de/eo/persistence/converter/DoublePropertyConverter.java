package de.eo.persistence.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

@Converter(autoApply = true)
public class DoublePropertyConverter implements AttributeConverter<DoubleProperty, Double> {

	@Override
	public Double convertToDatabaseColumn(DoubleProperty attribute) {
		if (attribute == null) {
			return null;
		}
		System.out.println(attribute.getValue());
		return attribute.getValue();
	}

	@Override
	public DoubleProperty convertToEntityAttribute(Double dbData) {
		return new SimpleDoubleProperty(dbData);
	}

}
