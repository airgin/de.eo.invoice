package de.eo.persistence.converter;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

@Converter(autoApply = true)
public class LocalDatePropertyConverter implements AttributeConverter<ObjectProperty<LocalDate>, Date> {

	@Override
	public Date convertToDatabaseColumn(ObjectProperty<LocalDate> localDateProperty) {
		if (localDateProperty == null || localDateProperty.get() ==  null) {
			return null;
		}
		final Instant instant = localDateProperty.get().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
		return Date.from(instant);
	}

	@Override
	public ObjectProperty<LocalDate> convertToEntityAttribute(Date value) {
		if (value == null) {
			return new SimpleObjectProperty<>();
		}
		final Instant instant = Instant.ofEpochMilli(value.getTime());
		return new SimpleObjectProperty<>(LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate());
	}
}
