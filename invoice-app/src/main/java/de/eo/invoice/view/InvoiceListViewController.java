package de.eo.invoice.view;

import java.net.URL;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;

import de.eo.entity.Invoice;
import de.eo.invoice.model.InvoiceModel;
import de.eo.invoice.statistic.CashAccountingChart;
import de.eo.persistence.dao.InvoiceDAO;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.chart.Chart;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 * Controller for invoice list view
 * @author eo
 *
 */
public class InvoiceListViewController implements Initializable {

	private InvoiceModel invoiceModel;

	@FXML
	private ListView<Invoice> invoiceList;

	@FXML
	private ComboBox<Invoice> invoiceYearFilter;

	private FilteredList<Invoice> invoiceListFilteredList;

	/**
	 * Initialize widgets from view
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		invoiceYearFilter.setConverter(new StringConverter<Invoice>() {

			@Override
			public String toString(Invoice value) {
				return Objects.toString(value.getDate().getValue().getYear());
			}

			@Override
			public Invoice fromString(String string) {
				return null;
			}
		});
		invoiceYearFilter.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Invoice>() {

			@Override
			public void changed(ObservableValue<? extends Invoice> observable, Invoice oldValue, Invoice newValue) {
				invoiceListFilteredList.setPredicate(currentInvoice -> currentInvoice.getDate().getValue()
						.getYear() == newValue.getDate().getValue().getYear());
			}

		});

		invoiceList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

		invoiceList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Invoice>() {

			@Override
			public void changed(ObservableValue<? extends Invoice> observable, Invoice oldValue, Invoice newValue) {
				invoiceModel.setSelectedIvoice(newValue);
				System.out.println(newValue);
			}
		});
		invoiceList.setCellFactory(new Callback<ListView<Invoice>, ListCell<Invoice>>() {

			@Override
			public ListCell<Invoice> call(ListView<Invoice> param) {
				ListCell<Invoice> cell = new ListCell<Invoice>() {
					@Override
					protected void updateItem(Invoice invoice, boolean bln) {
						super.updateItem(invoice, bln);
						if (invoice != null) {
							setText(invoice.getNumber().get() + " " + invoice.getReceiver().get());
						} else {
							setText("");
						}
					}
				};
				return cell;
			}
		});

		final ContextMenu contextMenu = new ContextMenu();
		MenuItem deleteMenuItem = new MenuItem("L�schen");
		deleteMenuItem.setOnAction(event -> {
			Optional<ButtonType> confirmResult = new Alert(Alert.AlertType.CONFIRMATION,
					"Wollen Sie die ausgew�hlten Rechnungen l�schen?").showAndWait();
			if (confirmResult.get() != ButtonType.OK) {
				return;
			}

			ObservableList<Invoice> selectedInvoices = invoiceList.getSelectionModel().getSelectedItems();
			InvoiceDAO invoiceDAO = new InvoiceDAO();
			invoiceDAO.remove(selectedInvoices);
			invoiceList.getItems().removeAll(selectedInvoices);
			invoiceList.getSelectionModel().clearSelection();
		});
		contextMenu.getItems().add(deleteMenuItem);

		MenuItem createChartMenuItem = new MenuItem("Statistik");
		createChartMenuItem.setOnAction(event -> {

			StackPane pane = new StackPane();
			CashAccountingChart barChart = new CashAccountingChart();
			Chart chart = barChart.createChart(invoiceList.getSelectionModel().getSelectedItems());
			pane.getChildren().add(chart);

			Stage dialogStage = new Stage();
			dialogStage.setTitle("Birthday Statistics");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			// dialogStage.initOwner(InvoiceListViewController.this);
			Scene scene = new Scene(pane);
			dialogStage.setScene(scene);

			dialogStage.show();

		});
		contextMenu.getItems().add(createChartMenuItem);

		invoiceList.setOnContextMenuRequested(event -> {
			contextMenu.show(invoiceList, event.getScreenX(), event.getScreenY());
			event.consume();
		});

		System.out.println("InvoiceListViewController initialized ...");

	}

	/**
	 * Set model and create filtering
	 * @param invoiceModel
	 */
	public void setInvoiceModel(InvoiceModel invoiceModel) {
		this.invoiceModel = invoiceModel;
		ObservableList<Invoice> invoiceItems = invoiceModel.getInvoiceList();
		invoiceListFilteredList = new FilteredList<>(invoiceItems, s -> true);
		invoiceList.setItems(invoiceListFilteredList);
		invoiceYearFilter.setItems(invoiceItems);
	}

}
