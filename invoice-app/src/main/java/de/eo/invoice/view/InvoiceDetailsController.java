package de.eo.invoice.view;

import java.net.URL;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import de.eo.entity.Invoice;
import de.eo.entity.InvoiceDetails;
import de.eo.javafx.custom.NumberTextField;
import de.eo.javafx.custom.tablecell.DatePickerCell;
import javafx.beans.property.ObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.DateStringConverter;
import javafx.util.converter.NumberStringConverter;
import lombok.Data;

@Data
public class InvoiceDetailsController implements Initializable {

	private Invoice invoice;

	@FXML
	private TextArea receiverTextArea;

	@FXML
	private TextField numberTextField;

	@FXML
	private DatePicker datePicker;

	@FXML
	private NumberTextField netInvoiceTextfield;

	@FXML
	private TextField vatTextfield;

	@FXML
	private TextField grossInvoiceTextfield;

	@FXML
	private TableView<InvoiceDetails> detailsTable;

	@FXML
	private TableColumn<InvoiceDetails, LocalDate> fromDateColumn;

	@FXML
	private TableColumn<InvoiceDetails, LocalDate> toDateColumn;

	@FXML
	private TableColumn<InvoiceDetails, Number> amountColumn;

	@FXML
	private TableColumn<InvoiceDetails, String> descriptionColumn;

	@FXML
	private TableColumn<InvoiceDetails, Number> unitPriceColumn;

	@FXML
	private TableColumn<InvoiceDetails, Number> priceColumn;

	private DateStringConverter dsc = new DateStringConverter();

	private final NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		initCellValuefactory();
		initCellFactory();
		initOnEditCommit();
	}

	private void initCellValuefactory() {
		fromDateColumn.setCellValueFactory(new PropertyValueFactory<>("fromDate"));
		toDateColumn.setCellValueFactory(new PropertyValueFactory<>("toDate"));
		amountColumn.setCellValueFactory(cellData -> cellData.getValue().getAmountPerUnit());
		descriptionColumn.setCellValueFactory(cellData -> cellData.getValue().getDescription());
		unitPriceColumn.setCellValueFactory(cellData -> cellData.getValue().getUnitPrice());
		priceColumn.setCellValueFactory(cellData -> cellData.getValue().getPrice());
	}

	private void initCellFactory() {
		fromDateColumn.setCellFactory(value -> new DatePickerCell<InvoiceDetails, LocalDate>());
		toDateColumn.setCellFactory(value -> new DatePickerCell<InvoiceDetails, LocalDate>());
		amountColumn.setCellFactory(
				TextFieldTableCell.<InvoiceDetails, Number> forTableColumn(new NumberStringConverter()));
		descriptionColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		unitPriceColumn.setCellFactory(
				TextFieldTableCell.<InvoiceDetails, Number> forTableColumn(new NumberStringConverter()));
	}

	private void initOnEditCommit() {
		fromDateColumn.setOnEditCommit(event -> {
			event.getRowValue().setFromDate(event.getNewValue());
		});

		toDateColumn.setOnEditCommit(event -> {
			event.getRowValue().setToDate(event.getNewValue());
		});

		amountColumn.setOnEditCommit(event -> {
			InvoiceDetails invoiceDetails = event.getRowValue();
			invoiceDetails.getAmountPerUnit().set(event.getNewValue().doubleValue());
			calculatePriceColumn(invoiceDetails);
			calculateNetInvoice();
		});

		unitPriceColumn.setOnEditCommit(event -> {
			InvoiceDetails invoiceDetails = event.getRowValue();
			invoiceDetails.getUnitPrice().set(event.getNewValue().doubleValue());
			calculatePriceColumn(invoiceDetails);
			calculateNetInvoice();
		});

	}

	public void bindBidirectional(ObjectProperty<Invoice> objectProperty) {
		invoice = objectProperty.get();
		if (invoice != null) {
			bindPropertys();
		}

		objectProperty.addListener(new ChangeListener<Invoice>() {

			@Override
			public void changed(ObservableValue<? extends Invoice> observable, Invoice oldValue, Invoice newValue) {
				unbindFields();
				invoice = newValue;
				if (newValue == null) {
					return;
				}

				bindPropertys();
			}
		});
	}

	private void bindPropertys() {
		receiverTextArea.textProperty().bindBidirectional(invoice.getReceiver());
		numberTextField.textProperty().bindBidirectional(invoice.getNumber());
		datePicker.valueProperty().bindBidirectional(invoice.getDate());

		netInvoiceTextfield.textProperty().bindBidirectional(invoice.getNetInvoice(), currencyFormat);
		vatTextfield.textProperty().bindBidirectional(invoice.getVat(), currencyFormat);
		grossInvoiceTextfield.textProperty().bindBidirectional(invoice.getGrossInvoice(), currencyFormat);

		List<InvoiceDetails> invoiceDetails = invoice.getInvoiceDetailsList();
		if (invoiceDetails == null) {
			invoiceDetails = new ArrayList<>();
		}
		ObservableList<InvoiceDetails> observableList = FXCollections.observableArrayList(invoiceDetails);
		detailsTable.setItems(observableList);

	}

	public void setEditable(boolean editable) {
		receiverTextArea.setEditable(editable);
		numberTextField.setEditable(editable);
		datePicker.setEditable(editable);
		datePicker.setDisable(!editable);
		netInvoiceTextfield.setEditable(editable);
		vatTextfield.setEditable(editable);
		grossInvoiceTextfield.setEditable(editable);
	}

	private void unbindFields() {
		if (invoice == null) {
			return;
		}

		receiverTextArea.textProperty().unbindBidirectional(invoice.getReceiver());
		receiverTextArea.setText(null);
		numberTextField.textProperty().unbindBidirectional(invoice.getNumber());
		numberTextField.setText(null);
		datePicker.setValue(null);
		netInvoiceTextfield.textProperty().unbindBidirectional(invoice.getNetInvoice());
		netInvoiceTextfield.setText(null);
		vatTextfield.textProperty().unbindBidirectional(invoice.getVat());
		vatTextfield.setText(null);
		grossInvoiceTextfield.textProperty().unbindBidirectional(invoice.getGrossInvoice());
		grossInvoiceTextfield.setText(null);

		detailsTable.setItems(null);
	}

	private void calculatePriceColumn(InvoiceDetails invoiceDetails) {
		double amount = invoiceDetails.getAmountPerUnit().get();
		double unitPrice = invoiceDetails.getUnitPrice().get();

		Double price = amount * unitPrice;
		invoiceDetails.getPrice().set(price);
	}

	private void calculateNetInvoice() {
		ObservableList<InvoiceDetails> items = detailsTable.getItems();
		double invoiceSum = 0;
		for (InvoiceDetails invoiceDetails : items) {
			double amountPerUnit = invoiceDetails.getAmountPerUnit().get();
			double unitPrice = invoiceDetails.getUnitPrice().get();
			if (amountPerUnit == 0 || unitPrice == 0) {
				continue;
			}

			invoiceSum += (amountPerUnit * unitPrice);
		}
		invoice.getNetInvoice().set(invoiceSum);
		invoice.getVat().set(invoiceSum * 0.19);
		invoice.getGrossInvoice().set(invoiceSum * 1.19);
	}
}
