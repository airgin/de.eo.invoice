package de.eo.invoice.create;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import de.eo.entity.Invoice;
import de.eo.entity.InvoiceDetails;
import de.eo.invoice.view.InvoiceDetailsController;
import de.eo.persistence.dao.InvoiceDAO;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * Controller for creating a new invoice
 * @author eo
 *
 */
public class InvoiceNewDialogController implements Initializable {

	@FXML
	private StackPane invoiceDetails;

	@FXML
	private InvoiceDetailsController invoiceDetailsController;

	private Stage dialogStage;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		invoiceDetailsController.bindBidirectional(new SimpleObjectProperty<Invoice>(new Invoice()));
//		invoiceDetailsController.getNetInvoiceTextfield().setEditable(false);
		invoiceDetailsController.getVatTextfield().setEditable(false);
		invoiceDetailsController.getGrossInvoiceTextfield().setEditable(false);
		invoiceDetailsController.getDetailsTable().setEditable(true);
	}

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	public void saveInvoice() {
		Invoice invoice = invoiceDetailsController.getInvoice();
		double netInvoice = invoice.getNetInvoice().get();
		invoice.getNetInvoice().set(netInvoice);
		invoice.getVat().set(netInvoice * 0.19);
		invoice.getGrossInvoice().set(netInvoice * 1.19);

		ObservableList<InvoiceDetails> invoiceDetailsList = invoiceDetailsController.getDetailsTable().getItems();
		invoice.setInvoiceDetailsList(new ArrayList<InvoiceDetails>(invoiceDetailsList));

		InvoiceDAO invoiceDAO = new InvoiceDAO();
		invoiceDAO.persist(invoice);

		dialogStage.close();
	}

	public void abort() {
		dialogStage.close();
	}

	/**
	 * Add a new disposition to details list
	 */
	public void addInvoiceDetails() {
		ObservableList<InvoiceDetails> items = invoiceDetailsController.getDetailsTable().getItems();
		items.add(new InvoiceDetails());
	}

	/**
	 * Remove selected dispositions from details list
	 */
	public void removeInvoiceDetails() {
		TableView<InvoiceDetails> detailsTable = invoiceDetailsController.getDetailsTable();
		InvoiceDetails selectedItem = detailsTable.getSelectionModel().getSelectedItem();
		if (selectedItem != null) {
			detailsTable.getItems().remove(selectedItem);
		}
	}

}
