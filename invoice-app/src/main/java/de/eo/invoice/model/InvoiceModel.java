package de.eo.invoice.model;

import de.eo.entity.Invoice;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import lombok.Data;

/**
 * Model class for appllication
 * @author eo
 *
 */
@Data
public class InvoiceModel {

	private ObservableList<Invoice> invoiceList;

	private ObjectProperty<Invoice> selectedInvoiceProperty = new SimpleObjectProperty<>();

	public void setSelectedIvoice(Invoice invoice) {
		selectedInvoiceProperty.set(invoice);
	}

}
