package de.eo.invoice.application;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import de.eo.entity.Invoice;
import de.eo.invoice.create.InvoiceNewDialogController;
import de.eo.invoice.model.InvoiceModel;
import de.eo.invoice.view.InvoiceDetailsController;
import de.eo.invoice.view.InvoiceListViewController;
import de.eo.persistence.dao.InvoiceDAO;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Main controller
 * @author eo
 *
 */
public class ApplicationController implements Initializable {

	@FXML
	private InvoiceListViewController invoiceListViewController;

	@FXML
	private StackPane invoiceDetails;

	@FXML
	private InvoiceDetailsController invoiceDetailsController;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		invoiceDetailsController.setEditable(false);
		selectInvoiceList();
	}

	/**
	 * Action handler to create a new invoice
	 */
	@FXML
	public void createInvoice() {
		FXMLLoader loader = new FXMLLoader(InvoiceNewDialogController.class.getResource("InvoiceNewDialog.fxml"));
		BorderPane page;
		try {
			page = (BorderPane) loader.load();
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		Stage dialogStage = new Stage();
		dialogStage.setTitle("Rechnung anlegen");
		dialogStage.initModality(Modality.APPLICATION_MODAL);
		Scene scene = new Scene(page);
		dialogStage.setScene(scene);

		InvoiceNewDialogController dialogController = loader.getController();
		dialogController.setDialogStage(dialogStage);

		dialogStage.showAndWait();

		selectInvoiceList();
	}

	private void selectInvoiceList() {
		InvoiceDAO invoiceDAO = new InvoiceDAO();
		List<Invoice> invoiceList = invoiceDAO.selectAll();
		InvoiceModel invoiceModel = new InvoiceModel();
		invoiceModel.setInvoiceList(FXCollections.observableArrayList(invoiceList));

		invoiceListViewController.setInvoiceModel(invoiceModel);
		invoiceDetailsController.bindBidirectional(invoiceModel.getSelectedInvoiceProperty());
	}

	public void terminate() {
		Platform.exit();
		System.exit(0);
	}
}
