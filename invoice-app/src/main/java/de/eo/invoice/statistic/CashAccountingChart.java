package de.eo.invoice.statistic;

import java.math.BigDecimal;
import java.util.Collection;

import de.eo.entity.Invoice;
import javafx.collections.FXCollections;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.Chart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

public class CashAccountingChart {

	public Chart createChart(Collection<Invoice> invoices) {

		BigDecimal netValue = BigDecimal.ZERO;
		BigDecimal vatValue = BigDecimal.ZERO;
		BigDecimal grossValue = BigDecimal.ZERO;

		for (Invoice invoice :invoices) {
			netValue = netValue.add(new BigDecimal(invoice.getNetInvoice().getValue()));
			vatValue = vatValue.add(new BigDecimal(invoice.getVat().getValue()));
			grossValue = grossValue.add(new BigDecimal(invoice.getGrossInvoice().getValue()));
		}

		CategoryAxis xAxis = new CategoryAxis();
		NumberAxis yAxis = new NumberAxis();
		BarChart<String, Number> chart = new BarChart<String, Number>(xAxis, yAxis);
		yAxis.setLabel("in EURO");

		XYChart.Series<String, Number> netSeries = new XYChart.Series<String, Number>("Netto",
				FXCollections.observableArrayList(new XYChart.Data<String, Number>("", netValue)));

		XYChart.Series<String, Number> vatSeries = new XYChart.Series<String, Number>("Mwst.",
				FXCollections.observableArrayList(new XYChart.Data<String, Number>("", vatValue)));

		XYChart.Series<String, Number> grossSeries = new XYChart.Series<String, Number>("Brutto",
				FXCollections.observableArrayList(new XYChart.Data<String, Number>("", grossValue)));

		chart.getData().addAll(netSeries, vatSeries, grossSeries);

		return chart;
	}

	public Chart createChart() {
		CategoryAxis xAxis = new CategoryAxis();
		NumberAxis yAxis = new NumberAxis();
		BarChart<String, Number> bc = new BarChart<String, Number>(xAxis, yAxis);
		xAxis.setCategories(FXCollections.<String> observableArrayList(""));
		xAxis.setTickMarkVisible(false);
		yAxis.setLabel("Performance");
		XYChart.Series<String, Number> series1 = new XYChart.Series<String, Number>("JavaFX",
				FXCollections.observableArrayList(new XYChart.Data<String, Number>("", 956)));
		XYChart.Series<String, Number> series2 = new XYChart.Series<String, Number>("Swing",
				FXCollections.observableArrayList(new XYChart.Data<String, Number>("", 789)));
		XYChart.Series<String, Number> series3 = new XYChart.Series<String, Number>("JavaScript",
				FXCollections.observableArrayList(new XYChart.Data<String, Number>("", 312)));
		bc.getData().addAll(series1, series2, series3);

		return bc;
	}

}
