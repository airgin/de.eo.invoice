package de.eo.javafx.custom.tablecell;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Objects;

import javafx.beans.binding.Bindings;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;

public class DatePickerCell<S, T> extends TableCell<S, LocalDate> {

	private final DatePicker datePicker;

	public DatePickerCell() {
		this.datePicker = new DatePicker();
		datePicker.setEditable(true);
		setGraphic(datePicker);
		datePicker.setOnAction(event -> {
			setText(datePicker.getEditor().getText());
			commitEdit((LocalDate) datePicker.getValue());
		});

		contentDisplayProperty().bind(
				Bindings.when(editingProperty()).then(ContentDisplay.GRAPHIC_ONLY).otherwise(ContentDisplay.TEXT_ONLY));

	}

	@Override
	protected void updateItem(LocalDate item, boolean empty) {
		super.updateItem(item, empty);
		if (empty) {
			setText(null);
			return;
		}
		String dateValue = "";
		if (item != null) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy", Locale.GERMANY);
			dateValue = formatter.format(item);
		}
		setText(dateValue);
	}

}
